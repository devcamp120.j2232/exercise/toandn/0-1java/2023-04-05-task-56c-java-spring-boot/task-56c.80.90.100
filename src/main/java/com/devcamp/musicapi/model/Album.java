package com.devcamp.musicapi.model;

import java.util.List;

public class Album {
    private String name;
    private List<String> song;

    public Album(String name, List<String> song) {
        this.name = name;
        this.song = song;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getSong() {
        return song;
    }

    public void setSong(List<String> song) {
        this.song = song;
    }

    @Override
    public String toString() {
        return "Album [name=" + name + ", song=" + song + "]";
    }
    
    


     
    
}
