package com.devcamp.musicapi.model;

public class BandMember extends Comperser {
    // thành viên ban nhạc
    private String instrument; // dụng cụ

    public BandMember(String firstname, String lastname, String stagename, String instrument) {
        super(firstname, lastname, stagename);
        this.instrument = instrument;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    @Override
    public String toString() {
        return "BandMember [instrument=" + instrument + "]";
    }

}
